'use strict';

const express = require('express');

// Constants
const PORT = 80;
const HOST = '0.0.0.0';


function buildMessage(method, path) {
    return {
        msg: "From expressjs",
        method,
        path
    };
}

// App
const app = express();
app.get('/*', (req, res) => {
    res.json(buildMessage('get', req.originalUrl));
});

app.post('/*', (req, res) => {
    res.json(buildMessage('post', req.originalUrl));
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);